/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstract_java;


public class TestGraphic {
    
    public static void main(String[] args) {
        
        MyGraphics myCircle = new MyCircle(10, 5, 5);
        MyRectangle myRectangle = new MyRectangle(50, 50, 5, 10);
        
        System.out.println("Area of Cirecle = "+myCircle.findArea());
        System.out.println("Circle Location(x,y) = "+myCircle.getX()+","+myCircle.getY());
        
        System.out.println("Area of Rectangle = "+myRectangle.findArea());
        System.out.println("Rectangle Location(x,y) = "+myRectangle.getX()+","+myRectangle.getY());
        
        //move
        MyGraphics.move(myCircle, 50, 50);
        System.out.println("Circle Location(x,y) = "+myCircle.getX()+","+myCircle.getY());

        MyGraphics.move(myRectangle, 100, 100);
        System.out.println("Rectangle Location(x,y) = "+myRectangle.getX()+","+myRectangle.getY());
    }
}
