/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstract_java;

//key word abstract in Netbeans >> ab+Tab
public abstract class MyGraphics {
    
    private int x;
    private int y;
    
    //abstract method
    public abstract void draw();
    public abstract double findArea();

    public MyGraphics() {
    }

    public MyGraphics(int x, int y) {
        this.x = x;
        this.y = y;
    }

//    public void move(int x, int y){// concrete method
//        
//        this.setX(x);
//        this.setY(y);
//    }
    
    public static void move(MyGraphics graphics, int x, int y){// concrete method
        
        graphics.setX(x);
        graphics.setY(y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
