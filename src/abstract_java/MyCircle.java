/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstract_java;

//key word extends in Netbeans >> ex+Tab
public class MyCircle extends MyGraphics{

    private double radius;

    public MyCircle() {
    }

    public MyCircle(double radius) {
        this.radius = radius;
    }

    public MyCircle(double radius, int x, int y) {
        super(x, y);
        this.radius = radius;
    }

    @Override
    public void draw() {
        
    }

    @Override
    public double findArea() {
        
        return (Math.PI)*radius*radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    
    
}
