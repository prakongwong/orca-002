/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interface_java;

/**
 *
 * @author Promptnow
 */
public class HPPrinter extends MyPrinter implements Scanner,Fax{

    @Override
    public void print() {
        System.out.println("HP Printing....");
    }

    @Override
    public void scan() {
        System.out.println("HP Scanning....");
    }

    @Override
    public void fax() {
        System.out.println("HP Faxing....");
    }
    
    
}
