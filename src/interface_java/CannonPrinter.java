/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interface_java;

/**
 *
 * @author Promptnow
 */
public class CannonPrinter extends MyPrinter implements Fax{

    @Override
    public void print() {
        System.out.println("Cannon Printing....");
    }

    @Override
    public void fax() {
        System.out.println("Cannon Faxing....");
    }
    
}
