/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interface_java;

/**
 *
 * @author Promptnow
 */
public class EpsonPrinter extends MyPrinter implements Scanner{

    @Override
    public void print() {
        System.out.println("Epson Printing....");
    }

    @Override
    public void scan() {
        System.out.println("Epson Scanning....");
    }
    
}
