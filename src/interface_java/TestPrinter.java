/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interface_java;

/**
 *
 * @author Promptnow
 */
public class TestPrinter {
    public static void main(String[] args) {
        MyPrinter hp = new HPPrinter();
        hp.print();
        
        MyPrinter epson = new EpsonPrinter();
        epson.print();
        
        System.out.println("---------------------------");
        
        Scanner hps = new HPPrinter();
        hps.scan();
        
        Fax scn = new CannonPrinter();
        scn.fax();
        
        System.out.println("---------------------------");
        
        HPPrinter hpm = (HPPrinter) hp;
        hpm.print();
        hpm.scan();
        hpm.fax();
        
        CannonPrinter cpm = (CannonPrinter) scn;
        cpm.print();
        cpm.fax();
    }
    
}
